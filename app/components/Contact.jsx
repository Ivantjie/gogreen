var React = require('react')
var Details = require('ContactDetails')
var CMap = require('ContactMap')

var Contact = React.createClass({
  render: function() {
    return (
      <div>
        <div className='small-12 columns'>
          <Details/>
          <CMap/>
        </div>
        <div className='gg-padder'></div>
        <div className='small-12 columns gg-home-line'></div>
      </div>
    )
  }
})

module.exports = Contact
