var React = require('react')

var Home = React.createClass({
  render: function() {
    return (
      <div>
        <div className='small-12 columns gg-home-image'></div>
        <div className='small-12 columns gg-padder'></div>
        <div className='small-12 columns gg-home-text'>
          <h4>Corporate, commercial and retail fit-out specialist</h4>
          <p className='gg-home-quote'>
            “Perfection is not attainable, but if we chase perfection we can catch excellence”
          </p>
        </div>
        <div className='small-12 columns gg-home-line'></div>
      </div>
    )
  }
})

module.exports = Home
