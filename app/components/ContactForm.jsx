var React = require('react')

var ContactForm = React.createClass({
  getInitialState: function() {
    return {
      name: '',
      mail: '',
      text: '',
      submit: false
    }
  },
  onFormSubmit: function(e) {
    e.preventDefault()
  },
  onNameChange: function(event) {

  },
  setErrorMessage: function() {

  },
  render: function() {
    return (
      <div>
        {this.setErrorMessage()}
        <form onSubmit={this.onFormSubmit}>
          <input type='text' value={this.state.name.value}/>
          <input type='text' value={this.state.mail.value}/>
          <input type='text' value={this.state.text.value}/>
        </form>
      </div>
    )
  }
})

module.exports = ContactForm
