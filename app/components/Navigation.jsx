var React = require('react')
var {Link, IndexLink} = require ('react-router')

var Nav = (props) => {
  return (
    <div>
      <div className='row'>
        <div className='small-12 columns gg-nav-logo'>
          <img src='/img/logo.jpg'/>
        </div>
      </div>

      <div className='row'>
        <div className='menu-centered'>
          <ul className='menu gg-nav-link'>
            <li>
              <IndexLink to="/" className='gg-nav-link' activeClassName='active' activeStyle={{fontWeight: 'bold'}}>HOME</IndexLink>
            </li>
            <li>
              <IndexLink to="/Contact" className='gg-nav-link gg-nav-link-right' activeClassName='active' activeStyle={{fontWeight: 'bold'}}>GET IN TOUCH</IndexLink>
            </li>
          </ul>
        </div>
      </div>

      <div className='row gg-padder show-for-medium'></div>
    </div>
  )
}

module.exports = Nav
