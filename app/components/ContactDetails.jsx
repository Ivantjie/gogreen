var React = require('react')

var ContactDetails = React.createClass({
  css: {
    details: {
      paddingTop: '1rem',
      paddingBottom: '2.4rem',
      textAlign: 'center'
    },
    header: {
      textAlign: 'center'
    },
    paragraph: {
      margin: '0rem'
    },
    spacer: {
      height: '40px'
    }
  },
  render: function() {
    return (
      <div>
        <div className='row' style={this.css.details}>

          <div style={this.css.item} className='medium-4 columns'>
            <h4>Office Hours</h4>
            <p style={this.css.paragraph}>
              Monday to Friday 8:30 to 17:00 <br />
              Saturdays closed
            </p>
          </div>

          <div style={this.css.spacer} className='show-for-small-only small-12 columns'></div>

          <div style={this.css.item} className='medium-4 columns'>
            <h4>Address</h4>
            <p style={this.css.paragraph}>
              Unit 12A . Bellville Business Park<br/>
              DJ Wood Way . Bellville
            </p>
          </div>

          <div style={this.css.spacer} className='show-for-small-only small-12 columns'></div>

          <div style={this.css.item} className='medium-4 columns'>
            <h4>General Contact</h4>
            <p style={this.css.paragraph}>
              Phone: <a href="tel:+27219171511">021 917 1511</a>
            </p>
            <p style={this.css.paragraph}>
              Email:  <a href="mailto:info@gogreenprojects.co.za">info@gogreenprojects.co.za</a>
            </p>
          </div>
        </div>
      </div>
    )
  }
})

module.exports = ContactDetails
