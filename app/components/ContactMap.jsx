var React = require('react')

var ContactMap = React.createClass({
  css: {
    background: {
      backgroundColor: '#ECECEC',
      height: '450px'
    }
  },
  getInitialState: function() {
    return ({mouseEnabled: false})
  },
  onClickHandler: function() {
    this.setState({mouseEnabled: true})
  },
  onMouseLeaveHandler: function() {
    this.setState({mouseEnabled: false})
  },
  render: function() {
    var cv = ""

    if (this.state.mouseEnabled === false) {
      cv = "overlay"
    }

    return (
      <div>
        <div className={cv} onClick={this.onClickHandler} onMouseLeave={this.onMouseLeave}></div>
          <iframe width="100%" height="480" frameBorder="0" onClick={function() {alert("pose")}} src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCEEMDzH0SIv42DYabRuBgWjmoa0Iuw_Jc&q=place_id:ChIJHeQp-3tazB0RcdOowlR3fXY"></iframe>
      </div>
    )
  }
})

module.exports = ContactMap
