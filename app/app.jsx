var ReactDOM = require('react-dom');
var React = require('react')
var {Route, Router, IndexRoute, hashHistory} = require('react-router')
var Main = require('Main')
var Home = require('Home')
var Contact = require('Contact')

//Load Foundation
require('style!css!foundation-sites/dist/css/foundation.min.css')

$(document).on('ready page:load', function() {
  $(function() {
    $(document).foundation();
  });
});

//App CSS
require('style!css!sass!applicationStyles')

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path='/' component={Main}>
      <IndexRoute component={Home}/>
      <Route path='/Contact' component={Contact}/>
    </Route>
  </Router>,
  document.getElementById('app')
);
