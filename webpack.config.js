var webpack = require('webpack')

module.exports = {
  entry: [
    'script!jquery/dist/jquery.min.js',
    'script!foundation-sites/dist/js/foundation.min.js',
    './app/app.jsx'
  ],
  externals: {
    jquery: 'jQuery',
    foundation: 'Foundation'
  },
  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery'
    })
  ],
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    alias:{
      Main: 'app/components/main.jsx',
      applicationStyles: 'app/styles/app.scss',
      Home: 'app/components/Home.jsx',
      Navigation: 'app/components/Navigation.jsx',
      Contact: 'app/components/Contact.jsx',
      ContactDetails: 'app/components/ContactDetails.jsx',
      ContactMap: 'app/components/ContactMap.jsx'
    },
    root: __dirname,
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  },
  devtool: 'eval-source-map"'
};
